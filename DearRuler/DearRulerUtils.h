#pragma once
#include <string>
#include <utility>

#include "ImGUI/imgui.h"

class RulerObject
{
public:
	RulerObject() = default;
	RulerObject(const ImVec4& color);
	virtual ~RulerObject() = default;


	RulerObject(const RulerObject& other) = default;

	RulerObject(RulerObject&& other) noexcept
		: color(other.color)
	{
	}

	RulerObject& operator=(const RulerObject& other)
	{
		if (this == &other)
			return *this;
		color = other.color;
		return *this;
	}

	RulerObject& operator=(RulerObject&& other) noexcept
	{
		if (this == &other)
			return *this;
		color = other.color;
		return *this;
	}


	friend bool operator==(const RulerObject& lhs, const RulerObject& rhs)
	{
		return lhs.color.x == rhs.color.x
		&& lhs.color.y == rhs.color.y
		&& lhs.color.z == rhs.color.z
		&& lhs.color.w == rhs.color.w;
	}

	friend bool operator!=(const RulerObject& lhs, const RulerObject& rhs)
	{
		return !(lhs == rhs);
	}

	virtual void AddingObject(const ImVec2& mouse_pos, const bool& finished) = 0;

	virtual void DrawSettings(const int& index) = 0;
	virtual void DrawSelected(ImDrawList* draw_list, const ImVec2& origin, const int& pulse_alpha, const bool& pulse) = 0;

	virtual std::string GetName() = 0;
protected:
	ImVec4 color{0, 0, 0, 0};
};

class RulerLine : public RulerObject
{
public:
	RulerLine() = default;
	RulerLine(const ImVec2& point_1, const ImVec2& point_2, const ImVec4& color, const float& thickness);
	~RulerLine() override = default;


	RulerLine(const RulerLine& other) = default;

	RulerLine(RulerLine&& other) noexcept
		: RulerObject(std::move(other)),
		  point_1(other.point_1),
		  point_2(other.point_2),
		  thickness(other.thickness)
	{
	}

	RulerLine& operator=(const RulerLine& other)
	{
		if (this == &other)
			return *this;
		RulerObject::operator =(other);
		point_1 = other.point_1;
		point_2 = other.point_2;
		thickness = other.thickness;
		return *this;
	}

	RulerLine& operator=(RulerLine&& other) noexcept
	{
		if (this == &other)
			return *this;
		point_1 = other.point_1;
		point_2 = other.point_2;
		thickness = other.thickness;
		return *this;
	}


	friend bool operator==(const RulerLine& lhs, const RulerLine& rhs)
	{
		return static_cast<const RulerObject&>(lhs) == static_cast<const RulerObject&>(rhs)
			&& lhs.point_1.x == rhs.point_1.x
			&& lhs.point_1.y == rhs.point_1.y
			&& lhs.point_2.x == rhs.point_2.x
			&& lhs.point_2.y == rhs.point_2.y
			&& lhs.thickness == rhs.thickness;
	}

	friend bool operator!=(const RulerLine& lhs, const RulerLine& rhs)
	{
		return !(lhs == rhs);
	}

	void AddingObject(const ImVec2& mouse_pos, const bool& finished) override;

	void DrawSettings(const int& index) override;
	void DrawSelected(ImDrawList* draw_list, const ImVec2& origin, const int& pulse_alpha, const bool& pulse) override;

	std::string GetName() override;
protected:
	ImVec2 point_1{0, 0};
	ImVec2 point_2{0, 0};
	float thickness{0.f};
};
