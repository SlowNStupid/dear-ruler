#include "DearRulerUtils.h"

RulerObject::RulerObject(const ImVec4& color)
{
	this->color = color;
}

RulerLine::RulerLine(const ImVec2& point_1, const ImVec2& point_2, const ImVec4& color, const float& thickness) : RulerObject(color)
{
	this->point_1 = point_1;
	this->point_2 = point_2;
	this->thickness = thickness;
}

void RulerLine::AddingObject(const ImVec2& mouse_pos, const bool& finished)
{
	if (!finished)
	{
		point_2 = mouse_pos;
	}
}

void RulerLine::DrawSettings(const int& index)
{
	ImGui::Text("Line #%d settings", index);
	ImGui::ColorPicker4("##selected", reinterpret_cast<float*>(&color),
	                    ImGuiColorEditFlags_AlphaBar | ImGuiColorEditFlags_PickerHueWheel |
	                    ImGuiColorEditFlags_DisplayRGB);
	ImGui::SliderFloat("Thickness##selected", &thickness, 1, 100, "%.2f", ImGuiSliderFlags_AlwaysClamp);
}

void RulerLine::DrawSelected(ImDrawList* draw_list, const ImVec2& origin, const int& pulse_alpha, const bool& pulse)
{
	draw_list->AddLine(ImVec2(origin.x + point_1.x, origin.y + point_1.y),
	                   ImVec2(origin.x + point_2.x, origin.y + point_2.y),
	                   IM_COL32(color.x * 255, color.y * 255, color.z * 255,
	                            color.w * 255), thickness);

	if (pulse)
	{
		draw_list->AddLine(ImVec2(origin.x + point_1.x, origin.y + point_1.y),
		                   ImVec2(origin.x + point_2.x, origin.y + point_2.y),
		                   IM_COL32(255 - (color.x * 255), 255 - (color.y * 255), 255 - (color.z * 255), pulse_alpha),
		                   thickness > 2.f ? thickness * 0.6f : thickness);
	}
}

std::string RulerLine::GetName()
{
	return "Line - ("
		+ std::to_string(static_cast<int>(color.x * 255.f)) + ", "
		+ std::to_string(static_cast<int>(color.y * 255.f)) + ", "
		+ std::to_string(static_cast<int>(color.z * 255.f)) + ", "
		+ std::to_string(static_cast<int>(color.w * 255.f)) + ")";
}