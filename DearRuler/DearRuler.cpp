// Dear ImGui: standalone example application for DirectX 11
// If you are new to Dear ImGui, read documentation from the docs/ folder + read the top of imgui.cpp.
// Read online: https://github.com/ocornut/imgui/tree/master/docs

#include "ImGUI/imgui.h"
#include "ImGUI/imgui_impl_win32.h"
#include "ImGUI/imgui_impl_dx11.h"
#include <d3d11.h>
#define DIRECTINPUT_VERSION 0x0800
#include <fstream>
#include <sstream>
#include <string>
#include <tchar.h>
#include <thread>
#include <vector>

#include "dwmapi.h"

#include "DearRulerUtils.h"

// Data
static ID3D11Device* g_pd3dDevice = nullptr;
static ID3D11DeviceContext* g_pd3dDeviceContext = nullptr;
static IDXGISwapChain* g_pSwapChain = nullptr;
static ID3D11RenderTargetView* g_mainRenderTargetView = nullptr;

// Forward declarations of helper functions
bool CreateDeviceD3D(HWND hWnd);
void CleanupDeviceD3D();
void CreateRenderTarget();
void CleanupRenderTarget();
LRESULT WINAPI WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

// Forward declarations of custom helper functions
void PassThroughClick(bool enable, HWND& hwnd);
void CaptureKeys(bool& p_capture_key, ImGuiIO& p_io, ImVector<int>& p_current_keys_settings,
                 ImVector<int>& p_key_pressed_tracker);
void SaveKeybinds(const ImVector<int>& menu_keys, const ImVector<int>& quit_keys);
void RestoreKeybinds(ImVector<int>& p_menu_keys, ImVector<int>& p_quit_keys);
std::string VirtualKeyToString(const int& virtualKeyCode);

// Main code
int main(int, char**)
{
	// Get virtual screen resolution
	const int virtual_screen_width = GetSystemMetrics(SM_CXVIRTUALSCREEN);
	const int virtual_screen_height = GetSystemMetrics(SM_CYVIRTUALSCREEN);

	// Create application window
	ImGui_ImplWin32_EnableDpiAwareness();
	WNDCLASSEX wc = {
		sizeof(WNDCLASSEX), CS_CLASSDC, WndProc, 0L, 0L, GetModuleHandle(nullptr), nullptr, nullptr, nullptr, nullptr,
		_T("DearRuler"),
		nullptr
	};
	::RegisterClassEx(&wc);
	HWND hwnd = ::CreateWindowEx(WS_EX_TOPMOST | WS_EX_TRANSPARENT, wc.lpszClassName, _T("DearRuler"), WS_POPUP, 0, 0,
	                             virtual_screen_width, virtual_screen_height, nullptr, nullptr, wc.hInstance, nullptr);

	// Move window
	SetWindowPos(hwnd, nullptr, GetSystemMetrics(SM_XVIRTUALSCREEN), GetSystemMetrics(SM_YVIRTUALSCREEN), 0, 0,
	             SWP_NOSIZE | SWP_NOZORDER);

	// Make window transparent
	SetLayeredWindowAttributes(hwnd, 0, 0, LWA_ALPHA);
	SetLayeredWindowAttributes(hwnd, 0, RGB(0, 0, 0), LWA_COLORKEY);

	// Extend window into client area
	MARGINS Margin = {-1, -1, -1, -1};
	DwmExtendFrameIntoClientArea(hwnd, &Margin);

	// Initial click pass through
	PassThroughClick(false, hwnd);

	// Initialize Direct3D
	if (!CreateDeviceD3D(hwnd))
	{
		CleanupDeviceD3D();
		::UnregisterClass(wc.lpszClassName, wc.hInstance);
		return 1;
	}

	// Show the window
	ShowWindow(hwnd, SW_SHOWDEFAULT);
	UpdateWindow(hwnd);

	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO();
	(void)io;

	// Setup Dear ImGui style
	ImGui::StyleColorsDark();

	// Setup Platform/Renderer backends
	ImGui_ImplWin32_Init(hwnd);
	ImGui_ImplDX11_Init(g_pd3dDevice, g_pd3dDeviceContext);

	// Our state
	ImVec4 clear_color = ImVec4(0.f, 0.f, 0.f, 0.f);
	bool capture_menu_key = false;
	bool capture_quit_key = false;
	bool pass_click = false;

	bool adding_object = false;

	ImVec4 current_color = ImVec4(0.f, 0.f, 0.f, 1.0f);
	float current_thickness = 5.f;
	std::vector<std::unique_ptr<RulerObject>> ruler_objects;

	int current_selected_line = -1;
	int current_selected_pulse_alpha = 0;
	bool pulsing_upward = true;

	ImVector<int> pressed_key_tracker;
	ImVector<int> menu_keys;
	ImVector<int> quit_keys;

	RestoreKeybinds(menu_keys, quit_keys);
	if (menu_keys.empty())
	{
		menu_keys.push_back(36);
	}

	if (quit_keys.empty())
	{
		quit_keys.push_back(35);
	}

	// Main loop
	MSG msg;
	ZeroMemory(&msg, sizeof(msg));
	while (msg.message != WM_QUIT)
	{
		std::chrono::steady_clock::time_point endTimePoint = std::chrono::steady_clock::now() +
			std::chrono::milliseconds(24);

		{
			ImVector<bool> all_keys_pressed;
			for (int key : quit_keys)
			{
				if (GetAsyncKeyState(key) < 0)
				{
					all_keys_pressed.push_back(true);
				}
			}

			if (all_keys_pressed.size() == quit_keys.size())
			{
				break;
			}
		}

		if (!capture_menu_key && !capture_quit_key)
		{
			ImVector<bool> all_keys_pressed;
			for (int key : menu_keys)
			{
				if (GetAsyncKeyState(key) < 0)
				{
					all_keys_pressed.push_back(true);
				}
			}

			if (all_keys_pressed.size() == menu_keys.size())
			{
				pass_click = !pass_click;
				PassThroughClick(pass_click, hwnd);

				bool still_pressing = true;
				while (still_pressing)
				{
					all_keys_pressed.clear();
					for (int key : menu_keys)
					{
						if (GetAsyncKeyState(key) < 0)
						{
							all_keys_pressed.push_back(true);
						}
					}

					still_pressing = all_keys_pressed.size() == menu_keys.size();
					std::this_thread::sleep_for(std::chrono::milliseconds(5));
				}
			}
		}

		// Poll and handle messages (inputs, window resize, etc.)
		if (::PeekMessage(&msg, nullptr, 0U, 0U, PM_REMOVE))
		{
			TranslateMessage(&msg);
			::DispatchMessage(&msg);
			continue;
		}

		// Start the Dear ImGui frame
		ImGui_ImplDX11_NewFrame();
		ImGui_ImplWin32_NewFrame();
		ImGui::NewFrame();

		// ImGui::ShowDemoWindow();

		if (!pass_click)
		{
			// Keybind capture window
			if (capture_menu_key)
			{
				CaptureKeys(capture_menu_key, io, menu_keys, pressed_key_tracker);
			}
			else if (capture_quit_key)
			{
				CaptureKeys(capture_quit_key, io, quit_keys, pressed_key_tracker);
			}

			// Main Window
			ImGui::SetNextWindowPos(ImVec2(ImGui::GetMousePos().x - 200, ImGui::GetMousePos().y - 387),
			                        ImGuiCond_FirstUseEver);
			ImGui::SetNextWindowSize(ImVec2(400.f, 775.f), ImGuiCond_FirstUseEver);
			ImGui::SetNextWindowSizeConstraints(ImVec2(200.f, 200.f), ImVec2(1200, 2325));
			ImGui::Begin("Menu", nullptr, ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoCollapse);

			// Menu bar
			if (ImGui::BeginMenuBar())
			{
				if (ImGui::BeginMenu("Menu"))
				{
					ImGui::MenuItem("DearRuler 0.5", nullptr, false, false);
					if (ImGui::BeginMenu("Options"))
					{
						// Menu keybind
						ImGui::Text("Toggle menu:");

						for (int i = 0; i < menu_keys.size(); i++)
						{
							if (i > 0)
							{
								ImGui::SameLine();
								ImGui::Text("+");
							}

							ImGui::SameLine();
							ImGui::Text("%s", VirtualKeyToString(menu_keys[i]).c_str());
						}

						ImGui::SameLine();
						if (ImGui::Button("Edit##menu") && !capture_menu_key)
						{
							pressed_key_tracker.clear();
							capture_menu_key = true;
						}

						// Quit keybind
						ImGui::Text("Quit:");

						for (int i = 0; i < quit_keys.size(); i++)
						{
							if (i > 0)
							{
								ImGui::SameLine();
								ImGui::Text("+");
							}

							ImGui::SameLine();
							ImGui::Text("%s", VirtualKeyToString(quit_keys[i]).c_str());
						}

						ImGui::SameLine();
						if (ImGui::Button("Edit##quit") && !capture_menu_key)
						{
							pressed_key_tracker.clear();
							capture_quit_key = true;
						}

						ImGui::EndMenu();
					}

					if (ImGui::MenuItem("Quit"))
					{
						break;
					}

					ImGui::EndMenu();
				}

				ImGui::EndMenuBar();
			}

			// Content of window
			ImGui::Text("New line settings");
			ImGui::ColorPicker4("##current", reinterpret_cast<float*>(&current_color),
			                    ImGuiColorEditFlags_AlphaBar | ImGuiColorEditFlags_PickerHueWheel |
			                    ImGuiColorEditFlags_DisplayRGB);
			ImGui::SliderFloat("Thickness##current", &current_thickness, 1, 100, "%.2f", ImGuiSliderFlags_AlwaysClamp);

			ImGui::NewLine();

			if (ImGui::ListBoxHeader("##item_list"))
			{
				for (int i = 0; i < static_cast<int>(ruler_objects.size()); i++)
				{
					char buf[64];
					sprintf_s(buf, "#%d - %s", i + 1, ruler_objects[i]->GetName().c_str());
					if (ImGui::Selectable(buf, current_selected_line == i))
					{
						current_selected_line = i;
					}
				}
				ImGui::ListBoxFooter();
			}

			ImGui::NewLine();

			if (!ruler_objects.empty() && current_selected_line > -1)
			{
				ruler_objects[current_selected_line]->DrawSettings(current_selected_line + 1);

				ImGui::NewLine();

				if (ImGui::Button("Remove Selected"))
				{
					ruler_objects.erase(ruler_objects.begin() + current_selected_line);
					current_selected_line--;
				}

				ImGui::SameLine(ImGui::GetContentRegionMax().x - 80);

				if (ImGui::Button("Remove All"))
				{
					ruler_objects.clear();
					current_selected_line = -1;
				}
			}

			ImGui::End();
		}
		else
		{
			current_selected_line = -1;
		}

		// Canvas
		{
			RECT clientRect;
			GetClientRect(hwnd, &clientRect);
			ImGui::SetNextWindowSize(ImVec2(static_cast<float>(clientRect.right - clientRect.left),
			                                static_cast<float>(clientRect.bottom - clientRect.top)));
			ImGui::SetNextWindowPos(ImVec2(0, 0));

			ImGui::Begin("##canvas", nullptr,
			             ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove |
			             ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoBackground |
			             ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoBringToFrontOnFocus);

			ImDrawList* draw_list = ImGui::GetBackgroundDrawList();

			ImVec2 canvas_size = ImGui::GetContentRegionAvail();

			ImGui::InvisibleButton("canvas", canvas_size,
			                       ImGuiButtonFlags_MouseButtonLeft | ImGuiButtonFlags_MouseButtonRight);
			const bool is_hovered = ImGui::IsItemHovered();
			const ImVec2 origin = ImGui::GetCursorScreenPos();
			const ImVec2 mouse_pos_in_canvas(io.MousePos.x - origin.x, io.MousePos.y - origin.y);

			if (is_hovered && !adding_object && ImGui::IsMouseClicked(ImGuiMouseButton_Left))
			{
				ruler_objects.emplace_back(std::make_unique<RulerLine>(RulerLine(mouse_pos_in_canvas, mouse_pos_in_canvas, current_color, current_thickness)));
				adding_object = true;
			}

			if (adding_object)
			{
				if (!ImGui::IsMouseDown(ImGuiMouseButton_Left))
				{
					adding_object = false;
				}

				ruler_objects.back()->AddingObject(mouse_pos_in_canvas, !adding_object);
			}

			for (int i = 0; i < static_cast<int>(ruler_objects.size()); i++)
			{
				if (!ruler_objects.empty() && current_selected_line > -1 && current_selected_line == i)
				{
					if (pulsing_upward)
					{
						current_selected_pulse_alpha += 5;
						if (current_selected_pulse_alpha >= 150)
						{
							current_selected_pulse_alpha = 150;
							pulsing_upward = false;
						}
					}
					else
					{
						current_selected_pulse_alpha -= 5;
						if (current_selected_pulse_alpha <= 50)
						{
							current_selected_pulse_alpha = 50;
							pulsing_upward = true;
						}
					}

					ruler_objects[i]->DrawSelected(draw_list, origin, current_selected_pulse_alpha, true);
				}
				else
				{
					ruler_objects[i]->DrawSelected(draw_list, origin, 255, false);
				}
			}
			draw_list->PopClipRect();

			ImGui::End();
		}


		// Rendering
		ImGui::Render();
		g_pd3dDeviceContext->OMSetRenderTargets(1, &g_mainRenderTargetView, nullptr);
		g_pd3dDeviceContext->ClearRenderTargetView(g_mainRenderTargetView, reinterpret_cast<float*>(&clear_color));
		ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());

		g_pSwapChain->Present(0, 0);

		std::this_thread::sleep_until(endTimePoint);
	}

	// Cleanup
	ImGui_ImplDX11_Shutdown();
	ImGui_ImplWin32_Shutdown();
	ImGui::DestroyContext();

	CleanupDeviceD3D();
	DestroyWindow(hwnd);
	::UnregisterClass(wc.lpszClassName, wc.hInstance);

	SaveKeybinds(menu_keys, quit_keys);

	return 0;
}

// Helper functions

bool CreateDeviceD3D(HWND hWnd)
{
	// Setup swap chain
	DXGI_SWAP_CHAIN_DESC sd;
	ZeroMemory(&sd, sizeof(sd));
	sd.BufferCount = 2;
	sd.BufferDesc.Width = 0;
	sd.BufferDesc.Height = 0;
	sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	sd.BufferDesc.RefreshRate.Numerator = 60;
	sd.BufferDesc.RefreshRate.Denominator = 1;
	sd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.OutputWindow = hWnd;
	sd.SampleDesc.Count = 1;
	sd.SampleDesc.Quality = 0;
	sd.Windowed = TRUE;
	sd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

	const UINT createDeviceFlags = 0;
	D3D_FEATURE_LEVEL featureLevel;
	const D3D_FEATURE_LEVEL featureLevelArray[2] = {D3D_FEATURE_LEVEL_11_0, D3D_FEATURE_LEVEL_10_0,};
	if (D3D11CreateDeviceAndSwapChain(nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, createDeviceFlags, featureLevelArray,
	                                  2,
	                                  D3D11_SDK_VERSION, &sd, &g_pSwapChain, &g_pd3dDevice, &featureLevel,
	                                  &g_pd3dDeviceContext) != S_OK)
		return false;

	CreateRenderTarget();
	return true;
}

void CleanupDeviceD3D()
{
	CleanupRenderTarget();
	if (g_pSwapChain)
	{
		g_pSwapChain->Release();
		g_pSwapChain = nullptr;
	}
	if (g_pd3dDeviceContext)
	{
		g_pd3dDeviceContext->Release();
		g_pd3dDeviceContext = nullptr;
	}
	if (g_pd3dDevice)
	{
		g_pd3dDevice->Release();
		g_pd3dDevice = nullptr;
	}
}

void CreateRenderTarget()
{
	ID3D11Texture2D* pBackBuffer;
	g_pSwapChain->GetBuffer(0, IID_PPV_ARGS(&pBackBuffer));
	g_pd3dDevice->CreateRenderTargetView(pBackBuffer, nullptr, &g_mainRenderTargetView);
	pBackBuffer->Release();
}

void CleanupRenderTarget()
{
	if (g_mainRenderTargetView)
	{
		g_mainRenderTargetView->Release();
		g_mainRenderTargetView = nullptr;
	}
}

// Forward declare message handler from imgui_impl_win32.cpp
extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

// Win32 message handler
LRESULT WINAPI WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	if (ImGui_ImplWin32_WndProcHandler(hWnd, msg, wParam, lParam))
		return true;

	switch (msg)
	{
	case WM_SIZE:
		if (g_pd3dDevice != nullptr && wParam != SIZE_MINIMIZED)
		{
			CleanupRenderTarget();
			g_pSwapChain->ResizeBuffers(0, static_cast<UINT>(LOWORD(lParam)), static_cast<UINT>(HIWORD(lParam)),
			                            DXGI_FORMAT_UNKNOWN, 0);
			CreateRenderTarget();
		}
		return 0;
	case WM_SYSCOMMAND:
		if ((wParam & 0xfff0) == SC_KEYMENU) // Disable ALT application menu
			return 0;
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	}
	return ::DefWindowProc(hWnd, msg, wParam, lParam);
}


// Custom helper function
void PassThroughClick(bool enable, HWND& hwnd)
{
	long exstyle = GetWindowLong(hwnd, GWL_EXSTYLE);
	if (enable)
	{
		exstyle |= WS_EX_LAYERED;
		SetWindowLong(hwnd, GWL_EXSTYLE, exstyle);
	}
	else
	{
		exstyle &= ~WS_EX_LAYERED;
		SetWindowLong(hwnd, GWL_EXSTYLE, exstyle);
		SetForegroundWindow(hwnd);
	}
}

void CaptureKeys(bool& p_capture_key, ImGuiIO& p_io, ImVector<int>& p_current_keys_settings,
                 ImVector<int>& p_key_pressed_tracker)
{
	ImVector<int> keys_pressed_currently;

	ImGui::SetNextWindowPos(ImVec2(ImGui::GetMousePos().x - 100, ImGui::GetMousePos().y - 100), ImGuiCond_Always);
	ImGui::SetNextWindowSize(ImVec2(200.f, 200.f), ImGuiCond_Always);
	ImGui::SetNextWindowFocus();
	if (ImGui::Begin("##keybind", nullptr,
	                 ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoMove |
	                 ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoNav))
	{
		ImGui::NewLine();
		ImGui::NewLine();
		ImGui::SetCursorPosX((ImGui::GetWindowWidth() / 2) - (ImGui::CalcTextSize("Press key(s) to bind").x / 2));
		ImGui::Text("Press key(s) to bind");
		ImGui::NewLine();
		ImGui::NewLine();

		for (int i = 0; i < IM_ARRAYSIZE(p_io.KeysDown); i++)
		{
			if (p_io.KeysDownDuration[i] >= 0.0f)
			{
				if (!p_key_pressed_tracker.contains(i))
				{
					p_key_pressed_tracker.push_back(i);
				}

				keys_pressed_currently.push_back(i);
				if (keys_pressed_currently.size() > 1)
				{
					ImGui::SameLine();
					ImGui::Text("+");
				}

				ImGui::SameLine();
				ImGui::Text("%s", VirtualKeyToString(i).c_str());
			}
		}

		if (p_key_pressed_tracker.size() != keys_pressed_currently.size())
		{
			p_capture_key = false;

			p_current_keys_settings = p_key_pressed_tracker;
		}

		ImGui::End();
	}
}

void SaveKeybinds(const ImVector<int>& menu_keys, const ImVector<int>& quit_keys)
{
	std::string menu_keys_string;
	for (int key : menu_keys)
	{
		menu_keys_string += std::to_string(key) + ":";
	}
	menu_keys_string.pop_back();

	std::string quit_keys_strings;
	for (int key : quit_keys)
	{
		quit_keys_strings += std::to_string(key) + ":";
	}
	quit_keys_strings.pop_back();

	std::ofstream config_file;
	config_file.open("keybinds", std::ios::out | std::ios::trunc);
	if (config_file.is_open())
	{
		config_file << menu_keys_string << std::endl;
		config_file << quit_keys_strings << std::endl;

		config_file.close();
	}
}

void RestoreKeybinds(ImVector<int>& p_menu_keys, ImVector<int>& p_quit_keys)
{
	std::ifstream config_file("keybinds", std::ios::in);

	if (config_file.is_open())
	{
		std::string menu_keys, quit_keys;
		config_file >> menu_keys >> quit_keys;

		std::string key;

		std::stringstream menu_stringstream(menu_keys);
		while (std::getline(menu_stringstream, key, ':'))
		{
			p_menu_keys.push_back(std::stoi(key));
		}

		std::stringstream quit_stringstream(quit_keys);
		while (std::getline(quit_stringstream, key, ':'))
		{
			p_quit_keys.push_back(std::stoi(key));
		}
	}
	config_file.close();
}

std::string VirtualKeyToString(const int& virtualKeyCode)
{
	UINT scan_code = MapVirtualKeyA(virtualKeyCode, MAPVK_VK_TO_VSC);

	char name[128];
	int result;

	switch (virtualKeyCode)
	{
	case VK_LBUTTON:
		return "Left mouse";
	case VK_RBUTTON:
		return "Right mouse";
	case VK_MBUTTON:
		return "Middle mouse";
	case VK_XBUTTON1:
		return "Mouse 4";
	case VK_XBUTTON2:
		return "Mouse 5";
	case VK_LEFT:
	case VK_UP:
	case VK_RIGHT:
	case VK_DOWN:
	case VK_RCONTROL:
	case VK_LWIN:
	case VK_RWIN:
	case VK_END:
	case VK_HOME:
	case VK_INSERT:
	case VK_DELETE:
	case VK_NUMLOCK:
		scan_code |= KF_EXTENDED;
	default:
		result = GetKeyNameTextA(scan_code << 16, name, 128);
		break;
	}

	if (result == 0)
	{
		return std::to_string(virtualKeyCode) + " (Cannot translate to key)";
	}

	return name;
}
